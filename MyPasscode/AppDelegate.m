//
//  AppDelegate.m
//  MyPasscode
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "AppDelegate.h"
#import "PasscodeViewController.h"

@interface AppDelegate()

@property (nonatomic) UIView *coverView;
@property (nonatomic) PasscodeViewController *passcodeVC;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if ([PasscodeViewController usePasscode]) {
        [self.passcodeVC showAnimated:NO];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (!self.coverView) {
        self.coverView = [PasscodeViewController screenshotPlaceholder];
        [self.window addSubview:self.coverView];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if ([PasscodeViewController usePasscode]) {
        [self.passcodeVC showAnimated:YES];
        
        [self.coverView removeFromSuperview];
        self.coverView = nil;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    application.applicationIconBadgeNumber = 0;
    [self.coverView removeFromSuperview];
    self.coverView = nil;
}


#pragma mark - Custom Methods -

- (PasscodeViewController *)passcodeVC {
    if (!_passcodeVC) {
        _passcodeVC = [PasscodeViewController passcodeForApplicationWindow:self.window];
    }
    return _passcodeVC;
}

@end
