//
//  ViewController.m
//  MyPasscode
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "ViewController.h"
#import "PasscodeKit/PasscodeViewController.h"
#import "PasscodeKit/States/SetNewCodeState.h"
#import "PasscodeKit/States/DeleteCodeState.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *useCodeSwitch;
@end

@implementation ViewController

- (IBAction)actShowPin:(id)sender {
    PasscodeViewController *vc = //[PasscodeViewController passcodeForApplicationWindow:nil withState:nil andConfiguration:nil];
    [PasscodeViewController passcodeWithDefaultState];
    
    [vc showAnimated:YES];
}

- (IBAction)actSwitchChanged:(UISwitch *)sender {
    
    BOOL switchState = sender.on;
    
    BVoidCompletion successBlock = ^(){
        [PasscodeViewController setUsePasscode:sender.on];
    };
    
    BVoidCompletion cancelBlock = ^(){
        [sender setOn:!switchState animated:YES] ;
    };
    
    BFailShouldHideBlockType failBlock = ^() {
        [sender setOn:!switchState animated:YES];
        return YES;
    };
    
    PasscodeBaseState *state = nil;
    if (switchState) {
        state = [SetNewCodeState new];
    }
    else {
        state = [DeleteCodeState new];
    }
    
    PasscodeViewController *passcodeVC = [PasscodeViewController passcodeWithState:state];
    passcodeVC.successCallback = successBlock;
    passcodeVC.cancelCallback = cancelBlock;
    passcodeVC.failCallback = failBlock;
    
    [passcodeVC showAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.useCodeSwitch.on = [PasscodeViewController usePasscode];
}

@end
