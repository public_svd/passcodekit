//
//  ChangeCodeState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "ChangeCodeState.h"

@implementation ChangeCodeState

- (NSString *)title {
    return @"Change passcode";
}

@end
