//
//  DeleteCodeState.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/31/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeBaseState.h"

@interface DeleteCodeState : PasscodeBaseState

@end
