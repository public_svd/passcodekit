//
//  DeleteCodeState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/31/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "DeleteCodeState.h"
#import "PasscodeConfiguration.h"
#import "PasscodeBaseRepository.h"

@implementation DeleteCodeState

- (NSString *)title {
    return @"Enter your passcode";
}

- (BOOL)canBeCanceled {
    return YES;
}

- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context {
    PasscodeBaseRepository *repo = [context configuration].repository;
    if (code.count == repo.passcode.count) {
        if ([code isEqualToArray:repo.passcode]) {
            [repo deletePasscode];
            [context passcodeDidSuccseed];
        }
        else {
            [context passcodeDidFail];
        }
    }
}


@end
