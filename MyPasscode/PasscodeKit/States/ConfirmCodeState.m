//
//  ConfirmCodeState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "ConfirmCodeState.h"
#import "PasscodeConfiguration.h"
#import "PasscodeBaseRepository.h"

@implementation ConfirmCodeState

- (NSString *)title {
    return @"Re-enter your new passcode";
}

- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context {
    if (code.count == self.passcode.count) {
        if ([code isEqualToArray:self.passcode]) {
            BOOL succses = [[context configuration].repository savePasscode:code];
            if (succses) {
                [context passcodeDidSuccseed];
            }
            else {
//TODO: process saving error
            }
        }
        else {
            [context passcodeDidFail];
        }
    }
}


@end
