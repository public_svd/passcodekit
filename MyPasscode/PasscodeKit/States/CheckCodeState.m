//
//  EnterCodeState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "CheckCodeState.h"
#import "PasscodeConfiguration.h"
#import "PasscodeBaseRepository.h"

@implementation CheckCodeState

- (NSString *)title {
    return @"Enter your passcode";
}

- (BOOL)canBeCanceled {
    return NO;
}

- (BOOL)allowTouchID {
    return YES;
}

- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context {
    PasscodeBaseRepository *repo = [context configuration].repository;
    if (code.count == repo.passcode.count) {
        if ([code isEqualToArray:repo.passcode]) {
            [context passcodeDidSuccseed];
        }
        else {
            [context passcodeDidFail];
        }
    }
}

@end
