//
//  NewCodeState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "SetNewCodeState.h"
#import "ConfirmCodeState.h"
#import "PasscodeConfiguration.h"

@implementation SetNewCodeState

- (NSString *)title {
    return @"Enter a passcode";
}

- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context {
    if (code.count == [context configuration].passcodeLength) {
        ConfirmCodeState *newState = [ConfirmCodeState new];
        newState.passcode = code;
        [context passcodeChangeStateTo:newState];
    }
}


@end
