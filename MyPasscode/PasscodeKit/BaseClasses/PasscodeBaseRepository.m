//
//  PaascodeBaseRepository.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeBaseRepository.h"

@implementation PasscodeBaseRepository

- (BOOL)hasPasscode {
    return NO;
}

- (NSArray *)passcode {
    return nil;
}

- (NSUInteger)failedAttemptsCount {
    NSAssert(false, @"Overload this in subclass");
    return 0;
}

- (NSTimeInterval)inputBlockTimeinterval {
    return 30.0;
}

- (NSTimeInterval)inputUnblockTimestamp {
    NSAssert(false, @"Overload this in subclass");
    return 0;
}

- (BOOL)deleteInputBlockTimestamp {
    NSAssert(false, @"Overload this in subclass");
    return 0;
}

- (BOOL)savePasscode:(NSArray *)passcode {
    NSAssert(false, @"Overload this in subclass");
    return NO;
}

- (BOOL)deletePasscode {
    NSAssert(false, @"Overload this in subclass");
    return NO;
}

- (BOOL)incrementFailedAttemptsCount {
    NSAssert(false, @"Overload this in subclass");
    return NO;
}

- (BOOL)deleteFailedAttemptsCount {
    NSAssert(false, @"Overload this in subclass");
    return NO;
}

- (BOOL)setInputBlockTimestamp {
    NSAssert(false, @"Overload this in subclass");
    return NO;
}


+ (void)reset {
    PasscodeBaseRepository *repo = [self new];
    [repo deleteInputBlockTimestamp];
    [repo deletePasscode];
    [repo deleteFailedAttemptsCount];
}

@end
