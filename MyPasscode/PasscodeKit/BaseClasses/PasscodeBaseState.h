//
//  PasscodeBaseState.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeContext.h"

@interface PasscodeBaseState : NSObject

- (NSString *)title;
- (BOOL)canBeCanceled;
- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context;
- (BOOL)allowTouchID;

@end
