//
//  PaascodeBaseRepository.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PasscodeBaseRepository : NSObject

@property (nonatomic, readonly) BOOL hasPasscode;
@property (nonatomic, readonly) NSArray *passcode;
@property (nonatomic, readonly) NSUInteger failedAttemptsCount;
@property (nonatomic, readonly) NSTimeInterval inputBlockTimeinterval;
@property (nonatomic, readonly) NSTimeInterval inputUnblockTimestamp;

- (BOOL)savePasscode:(NSArray *)passcode;
- (BOOL)deletePasscode;

- (BOOL)incrementFailedAttemptsCount;
- (BOOL)deleteFailedAttemptsCount;

- (BOOL)setInputBlockTimestamp;
- (BOOL)deleteInputBlockTimestamp;

+ (void)reset;

@end
