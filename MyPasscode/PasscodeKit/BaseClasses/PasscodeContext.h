//
//  PasscodeLockDelegate.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PasscodeBaseState;
@class PasscodeConfiguration;

@protocol PasscodeContext <NSObject>

- (PasscodeConfiguration *)configuration;
- (void)passcodeDidSuccseed;
- (void)passcodeDidFail;
- (void)passcodeChangeStateTo:(PasscodeBaseState *)newState;

@end
