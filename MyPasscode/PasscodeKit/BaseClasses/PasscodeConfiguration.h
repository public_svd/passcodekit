//
//  PasscodeConfiguration.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PasscodeBaseRepository;

@interface PasscodeConfiguration : NSObject

@property (nonatomic, readonly) PasscodeBaseRepository *repository;
@property (nonatomic, readonly) NSUInteger passcodeLength;
@property (nonatomic, readonly) NSUInteger maximumInccorectAttempts;
@property (nonatomic, readonly) BOOL useTouchID;

+ (instancetype)sharedInstance;

@end
