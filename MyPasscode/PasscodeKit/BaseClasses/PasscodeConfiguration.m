//
//  PasscodeConfiguration.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeConfiguration.h"
#import "KeychainRepository.h"

static PasscodeConfiguration *__sharedInstance = nil;

@implementation PasscodeConfiguration

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [PasscodeConfiguration new];
    });
    return __sharedInstance;
}

- (PasscodeBaseRepository *)repository {
    return [KeychainRepository new];
}

- (NSUInteger)passcodeLength {
    return 4;
}

- (NSUInteger)maximumInccorectAttempts {
    return 3;
}

- (BOOL)useTouchID {
    return YES;
}

@end
