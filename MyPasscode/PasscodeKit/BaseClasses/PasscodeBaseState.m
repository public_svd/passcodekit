//
//  PasscodeBaseState.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeBaseState.h"

@implementation PasscodeBaseState

- (NSString *)title {
    NSAssert(false, @"Overload this in subclass");
    return @"Base State";
}

- (NSString *)description {
    return @"";
}

- (BOOL)canBeCanceled {
    return YES;
}

- (void)applyPasscode:(NSArray *)code inContext:(id<PasscodeContext>)context {
    
}

- (BOOL)allowTouchID {
    return NO;
}

@end
