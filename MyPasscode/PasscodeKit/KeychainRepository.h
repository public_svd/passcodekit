//
//  KeychainRepository.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeBaseRepository.h"

@interface KeychainRepository : PasscodeBaseRepository

@end
