//
//  PasscodeViewController.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasscodeContext.h"

@class PasscodeBaseState;
@class PasscodeConfiguration;
@protocol PasscodeDelegate;


typedef NS_ENUM(NSUInteger, LockState) {
    LockStateNewCode,
    LockStateConfirmCode,
    LockStateCheckCode,
    LockStateChangeCode,
    LockStateRemoveCode
};

typedef void(^BVoidCompletion)();
typedef BOOL(^BFailShouldHideBlockType)();

@interface PasscodeViewController : UIViewController <PasscodeContext>

@property (nonatomic, copy) BVoidCompletion successCallback;
@property (nonatomic, copy) BFailShouldHideBlockType failCallback;
@property (nonatomic, copy) BVoidCompletion cancelCallback;

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window withState:(PasscodeBaseState *)state andConfiguration:(PasscodeConfiguration *)configuration;
+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window withState:(PasscodeBaseState *)state;

//State selected automatically on passcode existence and for main application window.
+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window;
+ (instancetype)passcodeWithState:(PasscodeBaseState *)state;
+ (instancetype)passcodeWithDefaultState;

+ (UIView *)screenshotPlaceholder;
+ (BOOL)usePasscode;
+ (void)setUsePasscode:(BOOL)usePasscode;

@end
