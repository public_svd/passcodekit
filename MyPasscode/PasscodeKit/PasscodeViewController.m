//
//  PasscodeViewController.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeViewController.h"
#import "PasscodeBaseState.h"
#import "PasscodeConfiguration.h"
#import "PasscodePinDigit.h"
#import "PasscodeButton.h"
#import "SetNewCodeState.h"
#import "ConfirmCodeState.h"
#import "CheckCodeState.h"
#import "ChangeCodeState.h"
#import "KeychainRepository.h"
#import <LocalAuthentication/LocalAuthentication.h>

static NSString *const kUsePasscodeKey = @"UsePasscodeKey";


@interface PasscodeViewController ()

@property (nonatomic) UIWindow *mainWindow;
@property (nonatomic) UIWindow *lockWindow;

@property (nonatomic, assign) BOOL presented;
@property (nonatomic) BOOL pinAnimatingNow;
@property (nonatomic) BOOL inputBlocked;

@property (nonatomic) PasscodeConfiguration *configuration;
@property (nonatomic) PasscodeBaseState *state;
@property (nonatomic) NSMutableArray *passcode;
@property (nonatomic) NSTimer *blockingTimer;

@property (nonatomic, readonly) NSUInteger attemptsCount;
@property (nonatomic, readonly) NSInteger remainedBlockingSeconds;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic) IBOutletCollection (PasscodePinDigit) NSArray *placeholders;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteSignButton;
@property (nonatomic, weak) IBOutlet UIButton *touchIDButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *placeholdersX;

@end


@implementation PasscodeViewController

#pragma mark - Initializers

- (instancetype)initWithState:(PasscodeBaseState *)state andConfiguration:(PasscodeConfiguration *)configuration {
    NSString *nibName = @"PasscodeLockView";
    self = [super initWithNibName:nibName bundle:nil];
    
    if (self) {
        self.state = state;
        self.configuration = configuration;        
    }
    return self;
}

+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window withState:(PasscodeBaseState *)state andConfiguration:(PasscodeConfiguration *)configuration {
    
    PasscodeViewController *passcodeVC = [[self alloc] initWithState:state andConfiguration:configuration];
    passcodeVC.mainWindow = window;
    return passcodeVC;
}

+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window withState:(PasscodeBaseState *)state {
    return [self passcodeForApplicationWindow:window withState:state andConfiguration:nil];
}

+ (instancetype)passcodeForApplicationWindow:(UIWindow *)window {
    return [self passcodeForApplicationWindow:window withState:[self guessState]];
}

+ (instancetype)passcodeWithState:(PasscodeBaseState *)state {
    return [self passcodeForApplicationWindow:nil withState:state];
}

+ (instancetype)passcodeWithDefaultState {
    return [self passcodeForApplicationWindow:nil withState:[self guessState]];
}

+ (UIView *)screenshotPlaceholder {
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    UIView * result = [appWindow snapshotViewAfterScreenUpdates:NO];
    UIView *blurView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    blurView.frame = result.bounds;
    [result addSubview:blurView];
    return result;
}

+ (PasscodeBaseState *)guessState {
    PasscodeBaseState *guessedState = nil;
    PasscodeConfiguration *configuration = [PasscodeConfiguration new];
    if ([configuration.repository hasPasscode]) {
        guessedState = [CheckCodeState new];
    }
    else {
        guessedState = [SetNewCodeState new];
    }
    return guessedState;
}

+ (BOOL)usePasscode {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:kUsePasscodeKey];
    return result;
}

+ (void)setUsePasscode:(BOOL)usePasscode {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:usePasscode forKey:kUsePasscodeKey];
    [userDefaults synchronize];
}

#pragma mark - ViewController's Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    
//    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
//    [notificationCenter addObserver:self selector:@selector(appWillEnterForegroundHandler:) name: UIApplicationWillEnterForegroundNotification object: nil];
//    [notificationCenter addObserver:self selector:@selector(appDidEnterBackgroundHandler:) name: UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
//    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
//    [notificationCenter removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
//    [notificationCenter removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [super viewDidDisappear:animated];
}

#pragma mark - Properties

- (NSMutableArray *)passcode {
    if (!_passcode) {
        _passcode = [NSMutableArray new];
    }
    return _passcode;
}

- (NSUInteger)attemptsCount {
    return self.configuration.repository.failedAttemptsCount;
}

- (NSInteger)remainedBlockingSeconds {
    NSInteger result = (NSInteger)ceil(self.configuration.repository.inputUnblockTimestamp - [[NSDate date] timeIntervalSince1970]);
    return (result > 0) ? result : 0;
}

- (UIWindow *)mainWindow {
    if (!_mainWindow) {
        _mainWindow = [UIApplication sharedApplication].keyWindow;
    }
    return _mainWindow;
}

- (UIWindow *)lockWindow {
    if (!_lockWindow) {
        _lockWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _lockWindow.windowLevel = 0;
        [_lockWindow makeKeyAndVisible];
    }
    return _lockWindow;
}

- (void)showAnimated:(BOOL)animated {
//TODO: Add animations
    if (!self.presented) {
        self.presented = YES;
        self.lockWindow.windowLevel = 2;
        self.lockWindow.hidden = NO;
        self.lockWindow.rootViewController = self;
        self.mainWindow.windowLevel = 1;
        [self.mainWindow endEditing:YES];
        if (self.remainedBlockingSeconds > 0) {
            [self blockInput];
        }
        [self useBiometrics];
    }
}

- (void)hideAnimated:(BOOL)animated {
    //TODO: Add animations
    if (self.presented) {
        __block PasscodeViewController *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.presented = NO;
            weakSelf.mainWindow.windowLevel = 1;
            [weakSelf.mainWindow makeKeyAndVisible];
            
            [weakSelf resetPasscode];
            weakSelf.lockWindow.windowLevel = 0;
            weakSelf.lockWindow.rootViewController = nil;
        });
    }
}

#pragma mark - Actions

- (IBAction)passcodeSignButtonTap:(PasscodeButton *)sender {
    if (!self.pinAnimatingNow && !self.inputBlocked) {
        [self addSign:sender.titleLabel.text];
    }
}

- (IBAction)cancelButtonTap:(UIButton *)sender {
    if (self.cancelCallback) {
        self.cancelCallback();
    }
    [self hideAnimated:YES];
}

- (IBAction)deleteSignButtonTap:(UIButton *)sender {
    [self deleteSign];
}

- (IBAction)touchIDButtonTap:(UIButton *)sender {
    [self useBiometrics];
}

#pragma mark - Stuff

- (void)useBiometrics {
    LAContext *authContext = [LAContext new];
    NSError *error = nil;
    BOOL allowTouchID = self.configuration.useTouchID && self.state.allowTouchID;
    __weak PasscodeViewController *weakSelf = self;
    if (allowTouchID && [authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [authContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"Enter passcode" reply:^(BOOL success, NSError * _Nullable error)
        {
            if (success) {
                [weakSelf passcodeDidSuccseed];
            }
        }];
    }
}

- (void)addSign:(NSString *)sign {
    if (self.passcode.count < self.configuration.passcodeLength) {
        [self.passcode addObject:sign];
        PasscodePinDigit *digit = self.placeholders[self.passcode.count - 1];
        [digit setState:DigitStateActive animated:YES];
    }
    if (!self.inputBlocked) {
        [self.state applyPasscode:[self.passcode copy] inContext:self];
    }
    self.deleteSignButton.enabled = (self.passcode.count > 0);
}

- (void)deleteSign {
    [self.passcode removeLastObject];
    PasscodePinDigit *digit = self.placeholders[self.passcode.count];
    [digit setState:DigitStateInactive animated:YES];
    self.deleteSignButton.enabled = (self.passcode.count > 0);
}

- (void)resetPasscode {
    [self.passcode removeAllObjects];
    [self allDigitsToState:DigitStateInactive];
    [self updateUI];
}

- (void)updateUI {
    self.titleLabel.text = self.state.title;
    NSString *descriptionText = self.state.description;
    if (self.attemptsCount > 0) {
        descriptionText = [NSString stringWithFormat:@"%lu Failed Passcode Attempt", (long)self.attemptsCount];
    }
    else if (self.remainedBlockingSeconds) {
        descriptionText = [NSString stringWithFormat:@"Try again in %lu sec", (long)self.remainedBlockingSeconds];
    }
    self.descriptionLabel.text = descriptionText;
    self.cancelButton.hidden = !self.state.canBeCanceled;
    self.deleteSignButton.enabled = (self.passcode.count > 0);
}

- (void)allDigitsToState:(DigitState)state {
    [self.placeholders enumerateObjectsUsingBlock:^(PasscodePinDigit * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setState:state animated:YES];
    }];
}

- (void)showWrongPassword {
    self.pinAnimatingNow = YES;
    self.deleteSignButton.enabled = NO;
    [self allDigitsToState:DigitStateError];
    self.placeholdersX.constant = -40.0f;
    [self.view layoutIfNeeded];
   
    [UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:0.2f initialSpringVelocity:0.0f options:0 animations:^{
        self.placeholdersX.constant = 0.0f;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self allDigitsToState:DigitStateInactive];
        self.pinAnimatingNow = NO;
    }];
}

- (void)blockInput {
    self.inputBlocked = YES;
    [self resetPasscode];
    [self.configuration.repository deleteFailedAttemptsCount];
    self.blockingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tryUnblockInput) userInfo:nil repeats:YES];
}

- (void)unblockInput {
    [self.blockingTimer invalidate];
    self.inputBlocked = NO;
    [self resetPasscode];
}

- (void)tryUnblockInput {
    if (self.remainedBlockingSeconds > 0) {
        self.descriptionLabel.text = [NSString stringWithFormat:@"Try again in %lu sec", (long)self.remainedBlockingSeconds];
    }
    else {
        [self unblockInput];
    }
}

#pragma mark - PasscodeContext protocol

- (PasscodeConfiguration *)configuration {
    return [PasscodeConfiguration sharedInstance];
}


- (void)passcodeDidSuccseed {
    if (self.successCallback) {
        self.successCallback();
    }
    [self.configuration.repository deleteFailedAttemptsCount];
    [self hideAnimated:YES];
}

- (void)passcodeDidFail {
    [self.configuration.repository incrementFailedAttemptsCount];
    self.descriptionLabel.text = [NSString stringWithFormat:@"%lu Failed Passcode attempt", (long)self.attemptsCount];
    if (self.attemptsCount >= self.configuration.maximumInccorectAttempts) {
        [self.configuration.repository deleteFailedAttemptsCount];
        if (self.failCallback) {
            if (self.failCallback()) {
                [self hideAnimated:YES];
            }
        }
        if ([self.state isMemberOfClass:[CheckCodeState class]]) {
            [self.configuration.repository setInputBlockTimestamp];
            [self blockInput];
        }
    }
    [self resetPasscode];
    [self showWrongPassword];
}

- (void)passcodeChangeStateTo:(PasscodeBaseState *)newState {
    self.state = newState;
    [self resetPasscode];
}


@end
