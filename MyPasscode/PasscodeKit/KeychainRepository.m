//
//  KeychainRepository.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright © 2016 Serhii Derhach. All rights reserved.
//

#import "KeychainRepository.h"
#import <Security/Security.h>

static NSString *const kPasscodeKey = @"pascode";
static NSString *const kInputBlockTimeKey = @"inputBlockTime";
static NSString *const kFailedAttemptsCountKey = @"failedAttemptsCount";
static NSString *const kDelimiter = @",";

@implementation KeychainRepository

- (BOOL)hasPasscode {
    return (self.passcode.count > 0);
}

- (NSArray *)passcode {
    NSString *code = [self stringForKey:kPasscodeKey];
    NSArray *result = [code componentsSeparatedByString:kDelimiter];
    
    if (result.count == 0) {
        result = nil;
    }
    return result;
}

- (NSUInteger)failedAttemptsCount {
    return [[self stringForKey:kFailedAttemptsCountKey] integerValue];
}

- (NSTimeInterval)inputUnblockTimestamp {
    NSTimeInterval result = 0.0;
    NSString *intervalString = [self stringForKey:kInputBlockTimeKey];
    if (intervalString.length > 0) {
        result = intervalString.doubleValue;
    }
    return result;
}

- (BOOL)savePasscode:(NSArray *)passcode {
    BOOL result = NO;
    if (passcode.count > 0) {
        NSString *codeString = [passcode componentsJoinedByString:kDelimiter];
        result = [self setString:codeString forKey:kPasscodeKey];
    }
    return result;
}

- (BOOL)deletePasscode {
    BOOL result = [self setString:nil forKey:kPasscodeKey];
    return result;
}

- (BOOL)incrementFailedAttemptsCount {
    NSString *currentCountString = [self stringForKey:kFailedAttemptsCountKey];
    NSInteger currentCount = currentCountString ? currentCountString.integerValue : 0;
    NSString *newCountString = [NSString stringWithFormat:@"%lu", (long)++currentCount];
    return [self setString:newCountString forKey:kFailedAttemptsCountKey];
}

- (BOOL)deleteFailedAttemptsCount {
    return [self setString:nil forKey:kFailedAttemptsCountKey];
}

- (BOOL)setInputBlockTimestamp {
    NSTimeInterval blockingTimestamp = [[NSDate date] timeIntervalSince1970] + self.inputBlockTimeinterval;
    NSString *intervalString = [NSString stringWithFormat:@"%f", blockingTimestamp];
    return [self setString:intervalString forKey:kInputBlockTimeKey];
}

- (BOOL)deleteInputBlockTimestamp {
    return [self setString:nil forKey:kInputBlockTimeKey];
}

#pragma mark - Private/Utility stuff
- (NSString*)currentApplicationName {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *applicationName = [bundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    if (!applicationName) {
        applicationName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
    }
    return applicationName;
}

- (NSMutableDictionary *)queryForKey:(NSString *)key {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    if (key.length > 0) {
        NSString *formattedKey = [NSString stringWithFormat:@"%@ - %@", [self currentApplicationName], key];
        NSDictionary *queryDictionary = @{(id)kSecClass : (id)kSecClassGenericPassword,
                                          (id)kSecAttrService : @"service",
                                          (id)kSecAttrAccount : formattedKey};
        [result addEntriesFromDictionary:queryDictionary];
    }
    return result;
}

- (BOOL)setString:(NSString *)string forKey:(NSString *)key {
    BOOL result = NO;
    NSMutableDictionary *queryDictionary = [self queryForKey:key];
    NSData *newData = [string dataUsingEncoding:NSUTF8StringEncoding];
    if (newData == nil) {
        OSStatus res = SecItemDelete((CFDictionaryRef) queryDictionary);
        result = (res == errSecSuccess);
    }
    else {
        OSStatus res = SecItemCopyMatching((CFDictionaryRef) queryDictionary, NULL);
        switch (res) {
            case errSecItemNotFound: {
                [queryDictionary setObject:newData forKey:(id)kSecValueData];
                res = SecItemAdd((CFDictionaryRef)queryDictionary, NULL);
            }
                break;
            case errSecSuccess: {
                NSDictionary *attributeDict = @{(id)kSecValueData : newData};
                res = SecItemUpdate((CFDictionaryRef)queryDictionary, (CFDictionaryRef)attributeDict);
            }
                break;
        }
        result = (res == errSecSuccess);
    }
    return result;
}

- (NSString *)stringForKey:(NSString *)key {
    NSString *result = nil;
    if (key.length > 0) {
        NSMutableDictionary *queryDictionary = [self queryForKey:key];
        [queryDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
        CFTypeRef newData = nil;
        OSStatus res = SecItemCopyMatching((CFDictionaryRef)queryDictionary, &newData);
        if (res == errSecSuccess) {
            result = [[NSString alloc] initWithData:(__bridge NSData *)newData encoding:NSUTF8StringEncoding];
        }
    }
    return result;
}


@end
