//
//  PasscodeButton.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright (c) 2016 Serhii Derhach. All rights reserved.
//

@import UIKit;

IB_DESIGNABLE
@interface PasscodeButton : UIButton

@property (nonatomic) IBInspectable UIColor *defaultBackgroudColor;
@property (nonatomic) IBInspectable UIColor *highlightBackgroudColor;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;

@end