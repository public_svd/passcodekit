//
//  PasscodeButton.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright (c) 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodeButton.h"

@interface PasscodeButton ()

@end

@implementation PasscodeButton

@synthesize defaultBackgroudColor = _defaultBackgroudColor;
@synthesize highlightBackgroudColor = _highlightBackgroudColor;
@synthesize borderColor = _borderColor;
@synthesize borderWidth = _borderWidth;
@synthesize cornerRadius = _cornerRadius;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupActions];
    }
    return self;
}

- (UIColor *)defaultBackgroudColor {
    if (!_defaultBackgroudColor) {
        _defaultBackgroudColor = self.backgroundColor;
    }
    return _defaultBackgroudColor;
}

- (void)setDefaultBackgroudColor:(UIColor *)defaultBackgroudColor {
    if (_defaultBackgroudColor != defaultBackgroudColor) {
        _defaultBackgroudColor = defaultBackgroudColor;
        [self setupView];
    }
}

- (UIColor *)highlightBackgroudColor {
    if (!_highlightBackgroudColor) {
        _highlightBackgroudColor = [UIColor clearColor];
    }
    return _highlightBackgroudColor;
}

- (void)setHighlightBackgroudColor:(UIColor *)highlightBackgroudColor {
    if (_highlightBackgroudColor != highlightBackgroudColor) {
        _highlightBackgroudColor = highlightBackgroudColor;
        [self setupView];
    }
}

- (UIColor *)borderColor {
    if (!_borderColor) {
        _borderColor = [UIColor whiteColor];
    }
    return _borderColor;
}

- (void)setBorderColor:(UIColor *)borderColor {
    if (_borderColor != borderColor) {
        _borderColor = borderColor;
        [self setupView];
    }
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    if (_cornerRadius != cornerRadius) {
        _cornerRadius = cornerRadius;
        [self setupView];
    }
}

#pragma mark - Overrides
- (CGSize)intrinsicContentSize {
    return CGSizeMake(80.0f, 80.0f);
}

#pragma mark - Private Stuff
- (void)setupActions {
    [self addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(touchUp) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchDragOutside];
}

- (void)touchDown {
    [self changeBackgroundColor:self.highlightBackgroudColor animated:YES];
}

- (void)touchUp {
    [self changeBackgroundColor:self.defaultBackgroudColor animated:YES];
}

- (void)changeBackgroundColor:(UIColor *)newColor animated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.1f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.backgroundColor = newColor;
                         } completion:nil];
    }
    
}

- (void)setupView {
    CALayer * caLayer = self.layer;
    caLayer.borderWidth = self.borderWidth;
    caLayer.borderColor = self.borderColor.CGColor;
    caLayer.cornerRadius = self.cornerRadius;
}


@end