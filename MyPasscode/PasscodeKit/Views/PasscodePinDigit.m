//
//  PasscodePinDigit.m
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright (c) 2016 Serhii Derhach. All rights reserved.
//

#import "PasscodePinDigit.h"

@implementation PasscodePinDigit

- (instancetype)init {
    if (self = [super init]) {
        [self setupView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupView];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(16.0f, 16.0f);
}

- (void)setActiveColor:(UIColor *)activeColor {
    if (_activeColor != activeColor) {
        _activeColor = activeColor;
        [self setupView];
    }
}

- (void)setInactiveColor:(UIColor *)inactiveColor {
    if (_inactiveColor != inactiveColor) {
        _inactiveColor = inactiveColor;
        [self setupView];
    }
}

- (void)setErrorColor:(UIColor *)errorColor {
    if (_errorColor != errorColor) {
        _errorColor = errorColor;
        [self setupView];
    }
}


- (void)setupView {
    CALayer *caLayer = self.layer;
    caLayer.cornerRadius = CGRectGetHeight(self.bounds) / 2.0f;
    caLayer.borderWidth = 1.0f;
    caLayer.borderColor = self.activeColor.CGColor;
    self.backgroundColor = self.inactiveColor;
}

- (void)setState:(DigitState)newState animated:(BOOL)animated {
    UIColor *backgroundColor = self.inactiveColor;
    UIColor *borderColor = self.activeColor;
    switch (newState) {
        case DigitStateInactive:
            backgroundColor = self.inactiveColor;
            borderColor = self.activeColor;
            break;
        case DigitStateActive:
            backgroundColor = self.activeColor;
            borderColor = self.activeColor;
            break;
        case DigitStateError:
            backgroundColor = self.errorColor;
            borderColor = self.errorColor;
            break;
    }

    
    [UIView animateWithDuration:0.3f delay:0.0f usingSpringWithDamping:1.0f initialSpringVelocity:0.0f options:0 animations:^{
        self.backgroundColor = backgroundColor;
        self.layer.borderColor = borderColor.CGColor;
    } completion:nil];
    
}

@end