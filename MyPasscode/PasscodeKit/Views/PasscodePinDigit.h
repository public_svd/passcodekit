//
//  PasscodePinDigit.h
//  PasscodeKit
//
//  Created by Serhii Derhach on 3/25/16.
//  Copyright (c) 2016 Serhii Derhach. All rights reserved.
//

@import UIKit;

typedef NS_ENUM(NSUInteger, DigitState) {
    DigitStateInactive,
    DigitStateActive,
    DigitStateError
};

IB_DESIGNABLE
@interface PasscodePinDigit : UIView

@property (nonatomic) IBInspectable UIColor *inactiveColor;
@property (nonatomic) IBInspectable UIColor *activeColor;
@property (nonatomic) IBInspectable UIColor *errorColor;

- (void)setState:(DigitState)newState animated:(BOOL)animated;

@end